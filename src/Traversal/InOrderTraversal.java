package Traversal;

import java.util.ArrayList;

public class InOrderTraversal implements Traversal{
	
	public ArrayList<Node> traverse(Node node) {
		
		ArrayList<Node> listNode = new ArrayList<Node>();	
		
		if(node.getLeft() != null){
			ArrayList<Node> ar = traverse(node.getLeft());
			
			for (Node x : ar){
				listNode.add(x);
			}
		}
		
		listNode.add(node);
		
		if (node.getRight() != null){
			ArrayList<Node> br = traverse(node.getRight());
			
			for (Node x : br){
				listNode.add(x);
			}
		}		
		return listNode;		
	}

}
