package Traversal;

import java.util.ArrayList;

public class PostOrderTraversal implements Traversal{
public ArrayList<Node> traverse(Node node) {
		
		ArrayList<Node> listNode = new ArrayList<Node>();	
		
		if(node.getLeft() != null){
			ArrayList<Node> ar = traverse(node.getLeft());
			
			for (Node x : ar){
				listNode.add(x);
			}
		}
			
		if (node.getRight() != null){
			ArrayList<Node> br = traverse(node.getRight());
			
			for (Node x : br){
				listNode.add(x);
			}
		}
		
		listNode.add(node);
		return listNode;		
	}
}
