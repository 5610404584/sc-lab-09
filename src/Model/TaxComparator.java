package Model;

import java.util.Comparator;

import Interface.Taxable;

public class TaxComparator implements Comparator<Taxable>{

	public int compare(Taxable other, Taxable other1) {
		
		// TODO Auto-generated method stub
		if(other.getTax()<other1.getTax()){
			return -1;
			
		}
		if(other.getTax()>other1.getTax()){
			return 1;
			
		}
		
		return 0;
		
	
	}
}
