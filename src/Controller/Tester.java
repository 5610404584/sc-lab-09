package Controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import Interface.Measurable;
import Interface.Taxable;

import Model.Company;

import Model.Data;
import Model.EarningComparator;
import Model.ExpenseComparator;
import Model.Person;
import Model.Product;
import Model.ProfitComparator;
import Model.TaxCalculator;
import Model.TaxComparator;

public class Tester {

	public static void main(String[] args) {
		Tester tester = new Tester();
		tester.testPersona1();
		tester.testProducta1();
		tester.testCompanya1();
		tester.testTaxCompare();
	}

	public void testPersona1(){
		ArrayList<Person> persons = new ArrayList<Person>();
		persons.add(new Person("Mark", 175, 200000));
		persons.add(new Person("Dream", 165, 400000));
		persons.add(new Person("Nam", 160, 100000));
		Collections.sort(persons);
		for(Person p : persons){
			System.out.println(p.toString());
			
		}
		
		System.out.println("");
		
	}
	public void testProducta1(){
		ArrayList<Product> products = new ArrayList<Product>();
		products.add(new Product("Computer", 19000));
		products.add(new Product("Radio", 25000));
		products.add(new Product("Telaphone", 15000));
		Collections.sort(products);
		for(Product p : products){
			System.out.println(p.toString());
			
		}
		
		System.out.println("");
		
	}
	public void testCompanya1(){
		ArrayList<Company> companies = new ArrayList<Company>();
		companies.add(new Company("Microsofe", 5000000, 700000));
		companies.add(new Company("Samsung", 2500000, 400000));
		companies.add(new Company("Seven", 3000000, 150000));
		Collections.sort(companies,new EarningComparator());
		for(Company p : companies){
			System.out.println(p.toString());
			
		}
		System.out.println("");
		
		Collections.sort(companies,new ExpenseComparator());
		for(Company p : companies){
			System.out.println(p);
			
		}
		System.out.println("");
		Collections.sort(companies,new ProfitComparator());
		for(Company p : companies){
			System.out.println(p.toString());
			
		}
		System.out.println("");
	}
	public void testTaxCompare(){
		ArrayList<Taxable> persons = new ArrayList<Taxable>();
		persons.add(new Person("Mark", 175, 200000));
		persons.add(new Person("Dream", 165, 400000));
		
		ArrayList<Taxable> companies = new ArrayList<Taxable>();
		companies.add(new Company("Microsofe", 5000000, 700000));
		companies.add(new Company("Samsung", 2500000, 400000));
		
		ArrayList<Taxable> products = new ArrayList<Taxable>();
		products.add(new Product("Computer", 19000));
		products.add(new Product("Radio", 25000));
		
		ArrayList<Taxable> allElements = new ArrayList<Taxable>();
		allElements.addAll(persons);
		allElements.addAll(companies);
		allElements.addAll(products);
		Collections.sort(allElements,new TaxComparator());
		for(Taxable p : allElements){
			System.out.println(p.toString());
			
		}
		
	}
	
}
